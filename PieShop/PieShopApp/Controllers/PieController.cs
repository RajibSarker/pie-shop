﻿using Microsoft.AspNetCore.Mvc;
using PieShopApp.Models.ViewModels;
using PieShopApp.Repositories.Interfaces;

namespace PieShopApp.Controllers
{
    public class PieController : Controller
    {
        private readonly IPieRepository _pieRepository;

        public PieController(IPieRepository pieRepository)
        {
            _pieRepository = pieRepository;
        }
        public IActionResult List()
        {
            PieViewModel list = new PieViewModel(_pieRepository.GetAllPieInformations, "Chees Cake");
            return View(list);
        }

        public IActionResult Details(int id)
        {
            return View(_pieRepository.GetPieById(id));
        }
    }
}
