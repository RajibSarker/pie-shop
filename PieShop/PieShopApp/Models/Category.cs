﻿namespace PieShopApp.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; } = string.Empty;
        public string? Description { get; set; }
        public IEnumerable<Pie>? Pies { get; set; }
    }
}
