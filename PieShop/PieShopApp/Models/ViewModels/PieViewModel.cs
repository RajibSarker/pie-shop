﻿namespace PieShopApp.Models.ViewModels
{
    public class PieViewModel
    {
        public IEnumerable<Pie> Pies { get; set; }
        public string? PieCategory { get; set; }
        public PieViewModel(IEnumerable<Pie> pies, string? category)
        {
            Pies = pies;
            PieCategory = category;
        }
    }
}
