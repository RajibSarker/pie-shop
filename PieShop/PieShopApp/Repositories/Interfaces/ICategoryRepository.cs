﻿using PieShopApp.Models;

namespace PieShopApp.Repositories.Interfaces
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetAllCategories();
    }
}
