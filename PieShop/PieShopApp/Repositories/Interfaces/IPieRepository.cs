﻿using PieShopApp.Models;

namespace PieShopApp.Repositories.Interfaces
{
    public interface IPieRepository
    {
        IEnumerable<Pie> GetAllPieInformations { get; }
        IEnumerable<Pie> GetPiesOfTheWeek { get; }
        Pie? GetPieById(int id);
    }
}
