﻿using PieShopApp.DataContext;
using PieShopApp.Models;
using PieShopApp.Repositories.Interfaces;

namespace PieShopApp.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _db;

        public CategoryRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public IEnumerable<Category> GetAllCategories()
        {
            //return new List<Category>()
            //{
            //            new Category(){
            //    Id = 1,
            //    CategoryName = "Avye",
            //     Description= "auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin"
            //},
            //new Category(){
            //        Id =2,
            //        CategoryName = "Cheyenne",
            //        Description= "ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non"
            //    },
            //    new Category(){
            //        Id = 3,
            //        CategoryName =  "Lysandra",
            //        Description= "ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate,"
            //    },
            //    new Category(){
            //        Id = 4,
            //        CategoryName =  "Ainsley",
            //        Description="Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla"
            //    },
            //new Category(){
            //        Id = 5,
            //        CategoryName =  "Emma",
            //        Description= "mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit."
            //    }
            //};

            return _db.Categories.OrderBy(c=>c.CategoryName);
        }
    }
}
