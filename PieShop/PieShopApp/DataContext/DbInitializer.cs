﻿namespace PieShopApp.DataContext
{
    public static class DbInitializer
    {
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            ApplicationDbContext db = applicationBuilder.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<ApplicationDbContext>();

            if (!db.Categories.Any())
            {
                //save some seed data
            }

            if (!db.Pies.Any())
            {
                // save some seed data here
            }
        }
    }
}
